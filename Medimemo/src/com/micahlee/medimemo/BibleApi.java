package com.micahlee.medimemo;

import java.net.URLEncoder;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

public class BibleApi {

	public static String getPassageText(String reference) throws Exception {
		HttpClient httpclient = new DefaultHttpClient(); 
		
		try {
			// Build request URI
			String restUri = "http://www.esvapi.org/v2/rest/"
					+ "passageQuery?key=IP"
					+ "&output-format=plain-text"
					+ "&include-headings=false"
					+ "&include-subheadings=false"
					+ "&include-footnotes=false"
					+ "&include-passage-references=false"
					+ "&include-passage-horizontal-lines=false"
					+ "&include-heading-horizontal-lines=false"
					+ "&line-length=0"
					+ "&include-verse-numbers=false"
					+ "&passage="
					+ URLEncoder.encode(reference, "UTF-8");			
			
			// Make http request
	        HttpGet request = new HttpGet(restUri);   
	        ResponseHandler<String> handler = new BasicResponseHandler();  
	        return httpclient.execute(request, handler);  
		} catch (Exception e) {
			throw new Exception("Failed to retrieve passage text.", e);
		} finally {
			httpclient.getConnectionManager().shutdown();
		}
	}
	
}
