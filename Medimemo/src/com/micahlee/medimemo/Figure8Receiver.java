package com.micahlee.medimemo;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.TextView;

public class Figure8Receiver extends BroadcastReceiver {
	public final static String ACTION_FIGURE8 = "com.micahlee.medimemo.figure8";
	public final static int NOTIFICATION_ID = 0x040404;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		
		if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
				// Setup Alarm
		      AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		      
		      Intent i = new Intent(Figure8Receiver.ACTION_FIGURE8);
		      PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
		      
		      //am.set(AlarmManager.RTC, System.currentTimeMillis(), pi);
		      am.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), 7200000, pi); //7200000
        } else {
		
			SharedPreferences settings = context.getSharedPreferences(MainActivity.PREFS, 0);
			
			// Show medimemo pop-up
			Intent i = new Intent(context, PassageActivity.class);	
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);  
			
			i.putExtra(PassageActivity.EXTRA_REFERENCE, settings.getString(MainActivity.PREF_REFERENCE, null));
			i.putExtra(PassageActivity.EXTRA_PASSAGE, settings.getString(MainActivity.PREF_PASSAGE, null));
			
			context.startActivity(i);
			
			// Show notification
			Notification notification = new Notification(R.drawable.logo_sm, String.format("Medimemo: %s",  settings.getString(MainActivity.PREF_REFERENCE, null)), System.currentTimeMillis());
			PendingIntent contentIntent = PendingIntent.getActivity(context, 1, i, PendingIntent.FLAG_ONE_SHOT);
			String contentTitle = "Medimemo";
			String contentText = String.format("Medimemo: %s",  settings.getString(MainActivity.PREF_REFERENCE, null));
			
			notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
			
			if(settings.getBoolean(MainActivity.PREF_VIBRATE, false)) {
				notification.vibrate = new long[] { 0, 1000 };
			}
			
			NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
			nm.notify(NOTIFICATION_ID, notification);
			
			// Setup next alert
			//AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
			  
			//i = new Intent(Figure8Receiver.ACTION_FIGURE8);
			//PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_ONE_SHOT);
			
			//am.set(AlarmManager.RTC, System.currentTimeMillis() + 10000, pi); //7200000
        }
		
	}
}