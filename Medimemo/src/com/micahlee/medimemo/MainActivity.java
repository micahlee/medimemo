package com.micahlee.medimemo;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	public static final String PREFS = "Medimemo.Preferences";
	public static final String PREF_PASSAGE = "Medimemo.Passage";
	public static final String PREF_REFERENCE = "Medimemo.Reference";
	public static final String PREF_VIBRATE = "Medimemo.Vibrate";
	
	Button btnChange;
	Button btnPreview;
	TextView txtPassage;
	CheckBox chkVibrate;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        loadViews();
        setupHandlers();
        
        // Restore preferences and passage
        SharedPreferences settings = getSharedPreferences(PREFS, 0);
        txtPassage.setText(settings.getString(PREF_REFERENCE, ""));
        chkVibrate.setChecked(settings.getBoolean(PREF_VIBRATE, false));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    protected void loadViews() {
    	btnChange = (Button)this.findViewById(R.id.btnChange);
    	txtPassage = (TextView)this.findViewById(R.id.txtPassage);
    	btnPreview = (Button)this.findViewById(R.id.btnPreview);
    	chkVibrate = (CheckBox)this.findViewById(R.id.chkVibrate);
    }
    
    protected void setupHandlers() {
    	btnChange.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

				alert.setTitle("Enter Passage");
				alert.setMessage("Enter Passage (eg. John 3:16)");

				// Set an EditText view to get user input 
				final EditText input = new EditText(MainActivity.this);
				input.setText(txtPassage.getText());
				alert.setView(input);

				alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					txtPassage.setText(input.getText());
					
					SharedPreferences settings = getSharedPreferences(PREFS, 0);
				    final SharedPreferences.Editor editor = settings.edit();
				    
				    editor.putString(PREF_REFERENCE, input.getText().toString());
				      
				      AsyncTask<String, Void, String> getPassageTask = new AsyncTask<String, Void, String>() {
				    		
							@Override
							protected String doInBackground(String... arg0) {
								try {
									return BibleApi.getPassageText(arg0[0]);
								} catch (Exception e) {
									Log.e("PassageActivity.Create", e.getMessage(), e);
									return null;
								}
							}
							
							@Override
							protected void onPostExecute(String result) {
								// We need an Editor object to make preference changes.
							      // All objects are from android.context.Context
							      
							      editor.putString(PREF_PASSAGE, result);
							      
							   // Commit the edits!
							      editor.commit();
							      btnPreview.setEnabled(true);
							      
							      // Setup Alarm
							      AlarmManager am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
							      
							      Intent i = new Intent(Figure8Receiver.ACTION_FIGURE8);
							      PendingIntent pi = PendingIntent.getBroadcast(MainActivity.this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
							      
							      //am.set(AlarmManager.RTC, System.currentTimeMillis(), pi);
							      am.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), 7200000, pi); //7200000
							}
				        	
				        };
				        
				        getPassageTask.execute(input.getText().toString());
				  }
				});

				alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				  public void onClick(DialogInterface dialog, int whichButton) {
				    // Canceled.
				  }
				});

				btnPreview.setEnabled(false);
				alert.show();
				// see http://androidsnippets.com/prompt-user-input-with-an-alertdialog				
			}
    		
    	});
    	
    	btnPreview.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this, PassageActivity.class);
				
				SharedPreferences settings = getSharedPreferences(PREFS, 0);
				
				i.putExtra(PassageActivity.EXTRA_REFERENCE, settings.getString(PREF_REFERENCE, null));
				i.putExtra(PassageActivity.EXTRA_PASSAGE, settings.getString(PREF_PASSAGE, null));
				
				MainActivity.this.startActivity(i);
			}
    	
    	});
    	
    	chkVibrate.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton arg0, boolean vibrate) {
				SharedPreferences settings = getSharedPreferences(PREFS, 0);
			    SharedPreferences.Editor editor = settings.edit();
			    editor.putBoolean(PREF_VIBRATE, vibrate);
			    editor.commit();				
			}
    	
    	});
    }
}
