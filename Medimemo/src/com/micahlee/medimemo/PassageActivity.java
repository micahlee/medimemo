package com.micahlee.medimemo;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class PassageActivity extends Activity {

	public static final String EXTRA_REFERENCE = "Reference";
	public static final String EXTRA_PASSAGE = "Passage";
	
	String mReference;
	String mPassage;
	
	TextView txtTitle;
	TextView txtPassage;
	Button btnDismiss;
	Button btnShare;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);       
        
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.activity_passage);
        loadViews();
        
        txtPassage.setMovementMethod(new ScrollingMovementMethod());
        
        mReference = getIntent().getStringExtra(EXTRA_REFERENCE);
        mPassage = getIntent().getStringExtra(EXTRA_PASSAGE);
        
        if(mReference == null) {
        	finish();
        }
        
        txtTitle.setText(String.format("Medimemo:\n%s", mReference));
        
        if(mPassage != null) {
        	txtPassage.setText(mPassage);
        } else {        
	        txtPassage.setText("Loading...");
	        
	        AsyncTask<String, Void, String> getPassageTask = new AsyncTask<String, Void, String>() {
	
				@Override
				protected String doInBackground(String... arg0) {
					try {
						return BibleApi.getPassageText(arg0[0]);
					} catch (Exception e) {
						Log.e("PassageActivity.Create", e.getMessage(), e);
						return null;
					}
				}
				
				@Override
				protected void onPostExecute(String result) {
					if(result == null) {
						txtPassage.setText("Failed to retrieve passage.");
					} else {
						txtPassage.setText(result);
					}
				}
	        	
	        };
	        
	        getPassageTask.execute(mReference);
        }
        
        setupHandlers();
        
	}
	
	protected void loadViews() {
		txtTitle = (TextView)findViewById(R.id.txtTitle);
		txtPassage = (TextView)findViewById(R.id.txtPassage);
		
		btnDismiss = (Button)findViewById(R.id.btnDismiss);
		btnShare = (Button)findViewById(R.id.btnShare);
	}
	
	protected void setupHandlers() {
		btnDismiss.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				NotificationManager nm = (NotificationManager)PassageActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
				nm.cancel(Figure8Receiver.NOTIFICATION_ID);
				
				finish();
			}			
		});
		
		btnShare.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
				sharingIntent.setType("text/plain");
				sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, String.format("Medimemo: %s", mReference));
				sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, txtPassage.getText());
				sharingIntent.putExtra("sms_body",txtPassage.getText());  
				
				startActivity(Intent.createChooser(sharingIntent, "Share via"));
				
			}			
		});
	}
}
